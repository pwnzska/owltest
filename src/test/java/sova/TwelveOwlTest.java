package sova;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.WithDriver;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@RunWith(SerenityRunner.class)
public class TwelveOwlTest {
    @Managed
    WebDriver driver;

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "D:\\webdriver\\chromedriver_2.33_32bit.exe");
        driver = new ChromeDriver();
    }

    @After
    public void endUp() {
        driver.quit();
    }

    @WithDriver("chrome")
    @Test
    public void checkUploadFileTest() {
        driver.get("http://sova-system.com");
        driver.findElement(By.id("fldLogin")).sendKeys("owldev");
        driver.findElement(By.id("fldPassword")).sendKeys("1o2w3l4s5y6s7t8e9m");
        driver.findElement(By.id("btnSubmit")).click();
        WebElement button = (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"__next\"]/div/div/div/div/div[2]/nav/div[2]/a")));
        button.click();
        WebElement dropZone = (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"dropZone\"]")));
        JavascriptExecutor js = (JavascriptExecutor)driver;
        js.executeScript("var hidden = document.querySelectorAll('.d-none'); hidden.forEach(function(element){element.classList.remove('d-none');});");
        WebElement file = (new WebDriverWait(driver, 10)).until(ExpectedConditions.visibilityOfElementLocated(By.id("fileField")));
        file.sendKeys("D:\\OwlTest\\notags.mp3");
        Boolean notDropZone = (new WebDriverWait(driver, 60)).until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[@id=\"dropZone\"]")));




    }
}


