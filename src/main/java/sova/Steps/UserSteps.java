package sova.Steps;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.WrongPageError;
import net.thucydides.core.steps.ScenarioSteps;

public class UserSteps extends ScenarioSteps {

    @Step
    public  <T extends PageObject> T open(Class<T> pageClass) {
        T page;
        try {
            page = getPages().onSamePage().currentPageAt(pageClass);
        } catch (WrongPageError e) {
            page = getPages().get(pageClass);
            page.open();
        }
        return page;
    }

}
