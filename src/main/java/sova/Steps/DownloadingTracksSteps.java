package sova.Steps;

import net.thucydides.core.steps.ScenarioSteps;
import sova.Pages.DownloadingTracksPage;

public class DownloadingTracksSteps extends ScenarioSteps {
    private DownloadingTracksPage loadTracks() {
        return new DownloadingTracksPage (getDriver());
    }
    
}
